package models;

import javax.swing.JComboBox;

/**
 * A class that creates the app's combo boxes
 * and provides methods to return each one
 * 
 * @author Oladimeji Ogunyoye
 *
 */
public class appComboBoxes 
{
   private JComboBox dayBox;
   private JComboBox monthBox;
   private JComboBox yearBox;
   private JComboBox intervalBox;
   
   /**
    * Constructor which creates each individual combo box
    * (Day, Month, Year and Interval)
    */
   public appComboBoxes()
   {
	   String[] days = new String[31];
	   String[] years = new String[43];
	   String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Dec"};
	   String[] interval = {"Monthly", "Daily", "Weekly"};
	   
	   int a = 1;
	   for(int i = 0; i < 31; i++)
	   {
		   days[i] = a + "";
		   a++;
	   }
	   
	   int z = 1970;
	   int x = 0;
	   
	   while(z <= 2012 && x <= 42)
	   {
		   years[x] = z + "";
		   x++; z++;
	   }
	   
	   dayBox = new JComboBox(days);
	   dayBox.setSelectedIndex(0);
	   monthBox = new JComboBox(months);
	   monthBox.setSelectedIndex(0);
	   yearBox = new JComboBox(years);
	   yearBox.setSelectedIndex(30);
	   intervalBox = new JComboBox(interval);
   }
   
   /**
    * A method that returns a combo box of days 1 to 31.
    * 
    * @return combo box of days
    */
   public JComboBox getDaysBox()
   {
	   return dayBox;
   }
   
   /**
    * A method that return a combo box of months January to December (abbreviated).
    * 
    * @return combo box of months
    */
   public JComboBox getMonthBox()
   {
	   return monthBox;
   }
   
   /**
    * A method that returns a combo box of years (in one year intervals) from 1970 to 2012.
    * 
    * @return a combo box of years (1970-2012)
    */
   public JComboBox getYearBox()
   {
	   return yearBox;
   }
   
   /**
    * A method that returns a combo box of user selected intervals (Monthly, Daily, Weekly)
    * 
    * @return a combo box of specified intervals
    */
   public JComboBox getIntervalBox()
   {
	   return intervalBox;
   }
}
