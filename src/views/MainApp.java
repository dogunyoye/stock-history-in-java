package views;

/**
 * A Stock Market Application that allows a user to 
 * input a stock symbol and gather information on the
 * history of that stock between specified periods of 
 * time. The information can also be gathered at 
 * specified intervals (Monthly, Weekly or Daily) and the
 * order of table can be modified to chronological
 * (the default is reverse chronological)
 * 
 * @author Oladimeji Ogunyoye
 *
 */
public class MainApp 
{
     public static void main(String[] args)
     {
    	 new AppFrame();
     }
}
