package views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.util.Calendar;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import models.appComboBoxes;
import models.lookupButtonAction;

/**
 * A class that sets up the fundamental aspects of the GUI 
 * that the user can interact with.
 * 
 * @author Oladimeji Ogunyoye
 *
 */

@SuppressWarnings("serial")
public class AppFrame extends JFrame 
{
    /**
     * AppFrame's constructor which sets up the application's window
     * and widgets contained in them.
     * 
     */
	public AppFrame()
	{
		super("Stock Market Application Professional");
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(390,190));
		appWidgets();
		pack();
		setVisible(true);
	}
	
	/**
	 * A method that sets up most of the app's widgets and
	 * attributes specific layout managers to each of them.
	 * 
	 */
	public void appWidgets()
	{
		JLabel stockSymbol = new JLabel("Stock Symbol: ");
		JTextField inField = new JTextField(15);
		JLabel begin = new JLabel("Begin: ");
		JLabel end = new JLabel("End: ");
		JLabel interval = new JLabel("Interval: ");
		JButton lookup = new JButton("Lookup");
		JCheckBox order = new JCheckBox("Chronological Order");
	    appComboBoxes boxImporter = new appComboBoxes();
        appComboBoxes boxImporter2 = new appComboBoxes();
	    JComboBox beginDay = boxImporter.getDaysBox();
	    JComboBox beginMonth = boxImporter.getMonthBox();
	    JComboBox beginYear = boxImporter.getYearBox();
	    
	    Calendar c = Calendar.getInstance();
	    
	    JComboBox endDay = boxImporter2.getDaysBox();
	    endDay.setSelectedIndex(c.get(Calendar.DATE)-1);
	    
	    JComboBox endMonth = boxImporter2.getMonthBox();
	    endMonth.setSelectedIndex(c.get(Calendar.MONTH));
	    
	    JComboBox endYear = boxImporter2.getYearBox();
	    endYear.setSelectedIndex(42);
	    
	    Container contentPane = getContentPane();
        JPanel mainPane = new JPanel();
        JPanel centrePane = new JPanel();
        JPanel xpane = new JPanel();
        JPanel zPane = new JPanel();
        JPanel yPane = new JPanel();
        add(mainPane, BorderLayout.NORTH);
        add(centrePane, BoxLayout.Y_AXIS);
        
        mainPane.add(stockSymbol);
        mainPane.add(inField);
        
        centrePane.add(begin);
        centrePane.add(beginDay);
        centrePane.add(beginMonth);
        centrePane.add(beginYear);
        
        centrePane.add(xpane, BorderLayout.PAGE_END);
        
        xpane.add(end);
        xpane.add(endDay);
        xpane.add(endMonth);
        xpane.add(endYear);
        
        zPane.add(interval);
        zPane.add(boxImporter.getIntervalBox());
        zPane.add(order);
        
        contentPane.add(yPane, BorderLayout.SOUTH);
        yPane.setLayout(new BoxLayout(yPane, BoxLayout.Y_AXIS));
        yPane.add(zPane);
        yPane.add(lookup);
        
        lookup.addActionListener(new lookupButtonAction(inField, beginDay, beginMonth, beginYear, endDay, 
        		endMonth, endYear,boxImporter.getIntervalBox(), order));
     
	}
}
